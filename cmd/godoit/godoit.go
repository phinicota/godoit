package main

import (
	"os"

	"github.com/BurntSushi/toml"
	logging "github.com/sirupsen/logrus"
	"gitlab.com/phinicota/godoit/pkg/godoit"
	"gopkg.in/alecthomas/kingpin.v2"
)

const (
	// Version is the current version string
	Version = "godoit v0.0.1"
)

var (
	dryrun    = kingpin.Flag("dryrun", "Show what would be done.").Short('n').Bool()
	verbosity = kingpin.Flag("verbose", "Verbosity level. Can be repeated: -vvv.").Short('v').Counter()
)

func init() {
	// Output to stdout instead of the default stderr
	logging.SetOutput(os.Stdout)
}

func main() {
	kingpin.Version(Version)
	kingpin.Parse()

	// TODO remove
	actualVerbosity := *verbosity
	if *dryrun {
		actualVerbosity = 10 // max out verbosity
	}
	callCtx := godoit.NewCallContext(actualVerbosity, *dryrun)

	var (
		log      = godoit.LogCtx(callCtx)
		filepath = "recipe.godoit"
	)

	log.Info("parse Recipe")
	recipe := godoit.NewRecipe()

	if _, err := toml.DecodeFile(filepath, &recipe); err != nil {
		log.Fatal(err)
	}

	log.Info("run Recipe")

	if callCtx.Verbosity > 2 {
		recipe.Trace = true
	}

	recipe.Parallel = true
	if err := recipe.Run(callCtx); err != nil {
		log.Fatal(err)
	}
	log.Info("finished running recipe")
}
