package godoit

import (
	"fmt"

	logging "github.com/sirupsen/logrus"
)

// LogCtx creates a log configured according
// the CallContext used
func LogCtx(ctx CallContext) *logging.Entry {
	log := logging.New()
	logEntry := log.WithFields(ctx.LogFields)

	switch {
	case ctx.Verbosity <= -2:
		log.SetLevel(logging.ErrorLevel)
	case ctx.Verbosity == -1:
		log.SetLevel(logging.WarnLevel)
	case ctx.Verbosity == 0:
		log.SetLevel(logging.InfoLevel)
	case ctx.Verbosity == 1:
		fallthrough
	default:
		log.SetLevel(logging.DebugLevel)
	}

	return logEntry
}

// CallContext establishes general parameters for the call
// like verbosity and dry-run mode
type CallContext struct {
	Verbosity int
	Dryrun    bool
	LogFields logging.Fields
}

// String satisfies Stringer
func (ctx CallContext) String() string {
	return fmt.Sprintf("verb:%v, dryrun:%v",
		ctx.Verbosity, ctx.Dryrun)
}

// CpCtx copies a CallContext fields
// avoid accumulating LogFields across contexts
func CpCtx(ctx CallContext) CallContext {
	tempFields := logging.Fields{}
	for key, val := range ctx.LogFields {
		tempFields[key] = val
	}
	return CallContext{
		Verbosity: ctx.Verbosity,
		Dryrun:    ctx.Dryrun,
		LogFields: tempFields}
}

// NewCallContext correctly initializes a CallContext
func NewCallContext(verbosity int, dryrun bool) CallContext {
	return CallContext{
		Verbosity: verbosity,
		Dryrun:    dryrun,
		LogFields: logging.Fields{}}
}

// LowerVerbContext returns a copy of the CallContext with decreased verbosity
func LowerVerbContext(callCtx CallContext) CallContext {
	return CallContext{
		Verbosity: callCtx.Verbosity - 1,
		Dryrun:    callCtx.Dryrun,
		LogFields: callCtx.LogFields,
	}
}
