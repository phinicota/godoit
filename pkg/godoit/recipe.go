package godoit

import (
	"bytes"
	"fmt"
	"os"
	"strings"
	"sync"
)

// Targets defines a collection of targets uniquely
// identified by their name
type Targets map[string]*Target

// Recipe is a collection of Targets with high-level
// operations that may need knowledge of all available
// Targets
type Recipe struct {
	Targets
	RecipeConfiguration
	ExecutionSteps   []ExecutionStep
	SetupSteps       []ExecutionStep
	Environment      []string
	env              map[string]string
	PretargetActions []Action
	PostargetActions []Action
	PrestepActions   []Action
	PosstepActions   []Action
	SetupTargets     map[string]struct{}
	TeardownTarget   *Target
	ExecOptions
}

// RecipeConfiguration specifies general parameters for recipe
type RecipeConfiguration struct {
	RequiredEnvVars map[string]struct{}
}

// ExecOptions provides customization to how recipe is executed
type ExecOptions struct {
	// HideWarnings will hide stderr of commands *unless*
	// exit status is different from 0
	HideWarnings bool

	// Parallel will execute independent targets concurrently
	Parallel bool

	// Equivalent to running set -x
	Trace bool
}

// NewRecipe correctly initializes a new Recipe
func NewRecipe() *Recipe {
	return &Recipe{
		Targets: make(Targets),
		RecipeConfiguration: RecipeConfiguration{
			RequiredEnvVars: map[string]struct{}{},
		},
		ExecutionSteps:   make([]ExecutionStep, 0),
		Environment:      []string{},
		env:              map[string]string{},
		PretargetActions: []Action{},
		PostargetActions: []Action{},
		PrestepActions:   []Action{},
		PosstepActions:   []Action{},
		SetupTargets:     map[string]struct{}{},
		TeardownTarget:   nil,
	}
}

// ExecutionStep contains the names of the targets to be executed
type ExecutionStep []string

// String satisfies String interface
func (execStep ExecutionStep) String() string {
	return strings.Join([]string(execStep), ", ")
}

// solveTargetTopologicalOrder returns the execution order of a given set of targets
// if it cannot solve, it will return an error
func solveTargetTopologicalOrder(targets map[string]*Target) ([]ExecutionStep, error) {
	// TODO simplify this!

	executionSteps := []ExecutionStep{}

	executedTargets := make(map[string]struct{})
	for len(executedTargets) < len(targets) {
		execStep := ExecutionStep{}
	nextTarget:
		for targetName, target := range targets {
			// TODO use a remainingTargets map
			// this will allow easier error reporting
			if _, ok := executedTargets[targetName]; ok {
				// skip executed targets!
				continue nextTarget
			}

			depsFullfilled := 0
			for _, depName := range target.TargetDeps {
				if _, ok := executedTargets[depName]; ok {
					depsFullfilled++
				}
			}

			// can be executed in this step
			if depsFullfilled == len(target.TargetDeps) {
				execStep = append(execStep, targetName)
			}
		}

		if len(execStep) == 0 {
			// TODO use remainingTargets and ExecutionSteps to
			// better report the error and find the circular dependency
			remainTargets := []string{}
			for targetName := range targets {
				if _, executed := executedTargets[targetName]; !executed {
					remainTargets = append(remainTargets, targetName)
				}
			}
			return executionSteps, fmt.Errorf("Recipe.SolveExecutionOrder: "+
				"Cannot find a solution for current recipe.\nTargets unsolved: %v",
				remainTargets)
		}

		executionSteps = append(executionSteps, execStep)

		// add to executed targets after it's been scheduled for execution
		for _, targetName := range execStep {
			executedTargets[targetName] = struct{}{}
		}
	}
	return executionSteps, nil
}

// SolveExecutionOrder resolves the order in which targets
// need to be executed. Returns error if it can't converge.
func (recipe *Recipe) SolveExecutionOrder() error {
	nonSetupTargets := map[string]*Target{}
	SetupTargets := map[string]*Target{}
	for targetName, target := range recipe.Targets {
		if _, isSetupTarget := recipe.SetupTargets[targetName]; isSetupTarget {
			SetupTargets[targetName] = target
		} else {
			nonSetupTargets[targetName] = target
		}
	}

	var err error
	if len(recipe.SetupSteps) == 0 {
		if recipe.SetupSteps, err = solveTargetTopologicalOrder(SetupTargets); err != nil {
			return err
		}
	}

	// only solve if not done yet.
	if len(recipe.ExecutionSteps) == 0 {
		if recipe.ExecutionSteps, err = solveTargetTopologicalOrder(nonSetupTargets); err != nil {
			return err
		}
	}

	return nil
}

// AddEnv appends an env var to recipe variables
func (recipe *Recipe) AddEnv(name, value string) {
	recipe.Environment = append(recipe.Environment,
		fmt.Sprintf("%v=%v", name, value))
	recipe.env[name] = value
}

// IsValid checks for consistency between Targets
func (recipe *Recipe) IsValid() error {

	// check required env vars are defined
	for varr := range recipe.RequiredEnvVars {
		_, definedOutside := os.LookupEnv(varr)
		_, definedInRecipe := recipe.env[varr]
		if !definedOutside && !definedInRecipe {
			return fmt.Errorf("Required env '%v' var not provided", varr)
		}
	}

	// check *all* targets are valid
	for _, target := range recipe.Targets {
		if err := target.IsValid(); err != nil {
			return err
		}
	}

	// check setup targets
	for targetName := range recipe.SetupTargets {
		// check every setup target exists
		if _, ok := recipe.Targets[targetName]; !ok {
			return fmt.Errorf("invalid recipe: "+
				"Setup has an undefined target: \"%v\".",
				targetName)
		}

		// setup targets can't depend on regular targets
		for _, dep := range recipe.Targets[targetName].TargetDeps {
			if _, ok := recipe.SetupTargets[dep]; !ok {
				return fmt.Errorf("invalid recipe: "+
					"Setup target \"%v\" depends on non-setup target \"%v\".",
					targetName, dep)
			}
		}
	}

	// check regular targets
	for targetName, target := range recipe.Targets {

		// regular targets are not in SetupTargets map
		if _, isSetupTarget := recipe.SetupTargets[targetName]; !isSetupTarget {

			for _, dep := range target.TargetDeps {

				// check every dep of every target exists
				if _, ok := recipe.Targets[dep]; !ok {
					return fmt.Errorf("invalid recipe: "+
						"\"%v\" depends on undefined target \"%v\".",
						targetName, dep)
				}

				// check they are not setup targets!
				if _, isSetupTarget := recipe.SetupTargets[dep]; isSetupTarget {
					return fmt.Errorf("invalid recipe: "+
						"Target \"%v\" can't depend on a setup target \"%v\".",
						targetName, dep)
				}
			}
		}
	}

	// check recipe is solvable
	return recipe.SolveExecutionOrder()
}

// Run executes targets ordered according to their dependencies
func (recipe *Recipe) Run(callCtx CallContext) error {

	if err := recipe.IsValid(); err != nil {
		return err
	}

	if err := addEnv(recipe.Environment); err != nil {
		return fmt.Errorf("failed to se environment: %v", err.Error())
	}

	log := LogCtx(callCtx)

	// setup targets
	execSteps := len(recipe.ExecutionSteps)
	setupSteps := len(recipe.SetupSteps)
	totalSteps := setupSteps + execSteps
	if recipe.TeardownTarget != nil {
		totalSteps++
	}
	globalStep := 0
	for k, execStep := range recipe.SetupSteps {
		globalStep++

		log.Infof("%v/%v - Setup (%v/%v): %v",
			globalStep, totalSteps,
			k+1, setupSteps,
			execStep)

		for _, targetName := range execStep {
			target := recipe.Targets[targetName]
			if err := target.Execute(CpCtx(callCtx),
				[]Action{}, []Action{},
				recipe.HideWarnings,
				recipe.Trace); err != nil {
				return TargetError{target.name, err}
			}
		}
	}

	var (
		preStepTarget = NewTarget("pre-Step")
		posStepTarget = NewTarget("pos-Step")
	)

	preStepTarget.Actions = append(preStepTarget.Actions, recipe.PrestepActions...)
	posStepTarget.Actions = append(posStepTarget.Actions, recipe.PosstepActions...)

	// regular targets
	for k, execStep := range recipe.ExecutionSteps {
		globalStep++

		log.Infof("%v/%v - Execute (%v/%v): %v",
			globalStep, totalSteps,
			k+1, execSteps,
			execStep)

		// before step actions
		if err := preStepTarget.Execute(CpCtx(callCtx), []Action{}, []Action{},
			recipe.HideWarnings,
			recipe.Trace); err != nil {
			return TargetError{preStepTarget.name, err}
		}

		// actually execute targets
		if !recipe.Parallel {
			for _, targetName := range execStep {
				target := recipe.Targets[targetName]
				if err := target.Execute(CpCtx(callCtx),
					recipe.PretargetActions,
					recipe.PostargetActions,
					recipe.HideWarnings,
					recipe.Trace); err != nil {
					return TargetError{target.name, err}
				}
			}
		} else {

			var (
				wg     sync.WaitGroup
				errors = NewTargetErrors()
			)

			for _, targetName := range execStep {
				target := recipe.Targets[targetName]

				var (
					pretargetActions = make([]Action, len(recipe.PretargetActions))
					postargetActions = make([]Action, len(recipe.PostargetActions))
				)
				// avoid race conditions with slices
				copy(pretargetActions, recipe.PretargetActions)
				copy(postargetActions, recipe.PostargetActions)

				wg.Add(1)
				go func() {
					if err := target.Execute(CpCtx(callCtx),
						pretargetActions,
						postargetActions,
						recipe.HideWarnings,
						recipe.Trace); err != nil {
						errors.Append(TargetError{target.name, err})
					}
					wg.Done()
				}()
			}
			wg.Wait()
			if errors.Len() != 0 {
				return errors
			}
		}

		// after step actions
		if err := posStepTarget.Execute(CpCtx(callCtx), []Action{}, []Action{},
			recipe.HideWarnings,
			recipe.Trace); err != nil {
			return TargetError{posStepTarget.name, err}
		}
	}

	if recipe.TeardownTarget != nil {
		globalStep++
		log.Infof("%v/%v - Teardown", globalStep, totalSteps)

		if err := recipe.TeardownTarget.Execute(CpCtx(callCtx),
			[]Action{},
			[]Action{},
			recipe.HideWarnings,
			recipe.Trace); err != nil {
			return TargetError{"Teardown", err}
		}
	}

	return nil
}

// TargetError defines an error that contains a target name
type TargetError struct {
	Target string
	err    error
}

// Error satisfies Error interface
func (te TargetError) Error() string {
	return fmt.Sprintf("target \"%v\" failed: %v",
		te.Target, te.err.Error())
}

// TargetErrors is used to return multiple target errors when recipe is
// executed in parallel
type TargetErrors struct {
	coll []TargetError
	mut  sync.Mutex
}

// NewTargetErrors correctly initializes collection of TargetError
func NewTargetErrors() *TargetErrors {
	return &TargetErrors{
		coll: []TargetError{},
	}
}

// Len returns len() of TargetErrors collection with thread safety
func (tee *TargetErrors) Len() int {
	tee.mut.Lock()
	defer tee.mut.Unlock()
	return len(tee.coll)
}

// Append adds a TargetError to collection with thread safety
func (tee *TargetErrors) Append(te TargetError) {
	tee.mut.Lock()
	defer tee.mut.Unlock()
	tee.coll = append(tee.coll, te)
}

// Error satisfies Error interface
func (tee *TargetErrors) Error() string {
	var b bytes.Buffer
	b.WriteString("Targets have returned the following errors:\n")
	for _, te := range tee.coll {
		b.WriteString("* " + te.Error() + "\n")
	}
	return b.String()
}
