package godoit

import (
	"errors"
	"fmt"

	"github.com/google/shlex"
)

// ErrEmptyCommand means an empty string was provided as command
var ErrEmptyCommand = errors.New("command can't be empty")

// Action represents shell command with arguments to be executed
type Action string

// IsValid performs checks and initializes an action before execution
func (action Action) IsValid() error {

	// check action is not empty
	if len(action) == 0 {
		return errors.New("action is empty")
	}

	// check it can be split using shlex
	if split, err := shlex.Split(string(action)); err != nil {
		return err
	} else if !(len(split) > 0) {
		return fmt.Errorf("shlex.Split action produced an empty split: %v", split)
	}

	return nil
}
