package godoit

import "testing"

var targetTestCases = []struct {
	testingTarget      Target
	depNames           []string
	expectedDepResult  []bool
	expectedIsValidNil bool
}{
	{
		Target{
			name:       "test1",
			TargetDeps: []string{"dep1", "dep2", "dep3"},
		},
		[]string{"dep4", "dep3", "adsfa", "123123lldfsdkf"},
		[]bool{false, true, false, false},
		true,
	},
	{
		Target{
			name:       "invalidtarget1", // depends on itself
			TargetDeps: []string{"invalidtarget1", "dep3"},
		},
		[]string{"invalidtarget1", "dep4"},
		[]bool{true, false},
		false,
	},
	{
		Target{
			name:       "invalidtarget2", // duplicated deps
			TargetDeps: []string{"dep3", "dep3"},
		},
		[]string{},
		[]bool{},
		false,
	},
}

func TestTargetListsDep(t *testing.T) {
	for _, test := range targetTestCases {
		for k, testingDepName := range test.depNames {
			expectedResult := test.expectedDepResult[k]
			if result := test.testingTarget.listsDep(testingDepName); expectedResult != result {
				t.Fatalf("Target.listsDep(\"%v\") failed.\nTargetDeps for target \"%v\" are: %v\nExpected: %v, Got: %v",
					testingDepName,
					test.testingTarget.name,
					test.testingTarget.TargetDeps,
					expectedResult, result)
			}
		}
	}
}

func TestTargetIsValid(t *testing.T) {
	for _, test := range targetTestCases {
		result := test.testingTarget.IsValid()
		if (result == nil && !test.expectedIsValidNil) ||
			(result != nil && test.expectedIsValidNil) {
			expectedString := "<non-nil>"
			if test.expectedIsValidNil {
				expectedString = "<nil>"
			}
			t.Fatalf("Target.IsValid() failed.\n"+
				"Target \"%v\" validity:\n"+
				"Expected: %v\nGot: %v",
				test.testingTarget.name,
				expectedString, result)
		}
	}
}
