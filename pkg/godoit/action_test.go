package godoit

import (
	"errors"
	"os"
	"testing"
	"testing/iotest"
)

var actionTestCases = []struct {
	action          Action
	expectedInitNil bool
	expectedCallErr error
}{
	{
		Action(""),
		false,
		ErrEmptyCommand,
	},
	{
		// 4 < 6
		Action("test 4 -lt 6"),
		true,
		nil,
	},
	{
		// 4 > 6
		Action("test 4 -gt 6"),
		true,
		errors.New("<non-nil>"),
	},
	{
		Action("test $testvar = random_value_123"),
		true,
		nil,
	},
	{
		Action("test -n $notemptyvar"),
		true,
		nil,
	},
}

func TestActions(t *testing.T) {

	for _, test := range actionTestCases {

		// init() test
		resultValid := test.action.IsValid()
		if (resultValid == nil && !test.expectedInitNil) ||
			(resultValid != nil && test.expectedInitNil) {
			expectedString := "<non-nil>"
			if test.expectedInitNil {
				expectedString = "<nil>"
			}
			t.Fatalf("Action \"%v\" failed init() test.\n"+
				"Expected: %v\nGot: %v",
				test.action,
				expectedString, resultValid)
		}

		// Call() test
		// TODO test output is correctly captured with iotest?
		if resultValid == nil {
			resultCall := Bash(
				[]string{"testvar=random_value_123", "notemptyvar=doesntmatter"},
				string(test.action),
				false,
				iotest.NewWriteLogger("stdout", os.Stdout),
				iotest.NewWriteLogger("stderr", os.Stderr))

			if (resultCall != test.expectedCallErr) &&
				!(resultCall != nil && test.expectedCallErr != nil) {
				// NOTE both non-nil means it's all good!
				// we don't test error string, we only care an error is returned.
				// error returned by Call() is not the same as
				// the one provided by expectedCallErr
				t.Fatalf("Action \"%v\" failed Call() test.\n"+
					"Expected: %v\nGot: %v",
					test.action,
					test.expectedCallErr, resultCall)
			}

		}
		// TODO add test for logged output
	}
}
