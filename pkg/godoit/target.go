package godoit

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"

	logging "github.com/sirupsen/logrus"
)

// bashFormatter adds '>' to command output
type bashFormatter struct {
	*logging.TextFormatter
}

func (bf *bashFormatter) Format(entry *logging.Entry) (out []byte, err error) {
	if entry.Level == logging.DebugLevel {
		entry.Message = "> " + entry.Message
	}
	return bf.TextFormatter.Format(entry)
}

// Target represents a task to be performed.
// Stores all the parsed information for each target.
type Target struct {
	name        string
	Actions     []Action
	TargetDeps  []string
	Environment []string
}

// NewTarget correctly initializes a Target structure
func NewTarget(name string) *Target {
	return &Target{
		name:        name,
		Actions:     []Action{},
		TargetDeps:  []string{},
		Environment: []string{"TARGET_NAME=" + name},
	}
}

// String satisfies Stringer interface
func (target Target) String() string {
	return target.name
}

// IsValid does some checks on target parameters.
func (target Target) IsValid() (err error) {

	// check target doesn't depend on itself
	if target.listsDep(target.name) {
		return fmt.Errorf("Target.IsValid:"+
			"target \"%v\" can't depend on itself.",
			target)
	}

	// check there are no duplicated deps
	tempMap := make(map[string]struct{})
	for _, dep := range target.TargetDeps {
		tempMap[dep] = struct{}{}
	}
	if len(tempMap) != len(target.TargetDeps) {
		return fmt.Errorf("Target.IsValid:"+
			"target \"%v\" can't have duplicated dependencies.",
			target)
	}

	return
}

// listsDep returns true if target has QuestionDepName in its listed target dependencies
func (target Target) listsDep(QuestionDepName string) bool {
	for _, ListedDepName := range target.TargetDeps {
		if ListedDepName == QuestionDepName {
			return true
		}
	}
	return false
}

// Execute runs target Actions sequentially
func (target Target) Execute(callCtx CallContext,
	preactions, posactions []Action,
	ignoreStderr, trace bool) error {

	totalActions := len(preactions) + len(target.Actions) +
		len(posactions)
	if totalActions == 0 {
		// nothing to do!
		return nil
	}

	callCtx.LogFields["target"] = target.name
	log := LogCtx(callCtx)

	if err := target.IsValid(); err != nil {
		return err
	}

	var b bytes.Buffer
	for k, action := range append(
		append(preactions, target.Actions...), posactions...) {
		if err := action.IsValid(); err != nil {
			log.Fatal(err)
		}

		cmdstr := fmt.Sprintf("%v", action)
		if !callCtx.Dryrun {
			log.Debug("+ ", cmdstr)
		} else {
			log.Debug("- ", cmdstr)
		}

		cmdstr = " { " + cmdstr + "; } "
		if k == 0 {
			b.WriteString(cmdstr)
		} else {
			b.WriteString(" && " + cmdstr)
		}
	}
	b.WriteString("\n")

	panicCloseErr := func(closer io.Closer) {
		if err := closer.Close(); err != nil {
			panic(err)
		}
	}

	logBash := LogCtx(LowerVerbContext(callCtx))
	logBash.Logger.SetFormatter(&bashFormatter{
		TextFormatter: &logging.TextFormatter{}})

	out := logBash.WriterLevel(logging.DebugLevel)
	defer panicCloseErr(out)

	var errout io.Writer
	if ignoreStderr {
		errout = ioutil.Discard
	} else {
		errout = logBash.WriterLevel(logging.WarnLevel)
		defer panicCloseErr(errout.(io.Closer))
	}

	if !callCtx.Dryrun {
		return Bash(target.Environment,
			b.String(),
			trace,
			out, errout)
	}
	return nil
}
