package godoit

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"os/exec"
	"strings"

	logging "github.com/sirupsen/logrus"
	logger "gitlab.com/phinicota/godoit/pkg/log"
)

const maxTokenSizeLog = 1024 * 1024

var bashPath = func() string {
	path, err := exec.LookPath("bash")
	if err != nil {
		panic("can't find bash in PATH!")
	}
	return path
}()

// Bash executes cmd in bash -c "$cmd"
func Bash(env []string, cmd string, trace bool,
	out, errout io.Writer) error {

	if trace {
		cmd = "set -x;" + cmd
	}

	cmdExec := exec.Command(bashPath, []string{"-c", cmd}...)
	cmdExec.Env = append(os.Environ(), env...)

	// fulldump stores stderr and stdout
	fulldump := bytes.NewBufferString("")
	fulldump.Grow(32 << 10) // 32.768 kB

	// stdout
	cmdExec.Stdout = io.MultiWriter(fulldump, out)

	// stderr
	buf := bytes.NewBufferString("")
	cmdExec.Stderr = io.MultiWriter(fulldump, buf, errout)

	if err := cmdExec.Run(); err != nil {
		// executed command error
		runErr := errors.New("stderr: " + buf.String() +
			"\nreason: " + err.Error())

		// TODO stderr and stdout don't seem to be ordered
		// chronologically in dump file

		// open dump file
		dumpfile, ert := ioutil.TempFile("", "dump_stderr_")
		if ert != nil {
			return errors.New(runErr.Error() +
				"; and failed to open dump file: " +
				ert.Error())
		}

		// actually dump log
		_, errd := fulldump.WriteTo(dumpfile)
		if errd != nil {
			return errors.New(runErr.Error() +
				"; and failed to dump file: " +
				errd.Error())
		}

		return errors.New(runErr.Error() +
			"; full log available at: " +
			dumpfile.Name())
	}

	return nil
}

func addEnv(env []string) error {
	for _, varr := range env {
		splitted := strings.SplitN(varr, "=", 2)
		if len(splitted) == 1 {
			return fmt.Errorf("failed parsing environment variable: \"%v\"", varr)
		}
		varname := splitted[0]
		varval := splitted[1]
		if err := os.Setenv(varname, varval); err != nil {
			return err
		}
	}

	return nil
}

// LoggedBash wraps the execution of a shell command
// using a logging infrastructure to capture its output.
func LoggedBash(
	env []string,
	cmd string,
	dryrun, trace bool,
	log *logging.Entry) error {

	// dryrun mode
	dryrunChar := "$"
	if dryrun {
		dryrunChar = "-"
	}
	log.Infof("%v %v", dryrunChar, cmd)

	// loggedCmd := cmd
	// if len(cmd) > 20 {
	// 	loggedCmd = cmd[:20]
	// }
	// cmdLogEntry := log.WithFields(
	// 	logging.Fields{
	// 		"cmd": loggedCmd,
	// 	})

	// TODO fix this custom buffersize hack
	// related: https://github.com/sirupsen/logrus/issues/564
	out := logger.CustomWriterLevel(logging.DebugLevel, maxTokenSizeLog)
	errout := logger.CustomWriterLevel(logging.WarnLevel, maxTokenSizeLog)
	logger.SetLevel(log.Logger.Level)

	panicCloseErr := func(closer io.Closer) {
		if err := closer.Close(); err != nil {
			panic(err)
		}
	}

	defer panicCloseErr(errout)
	defer panicCloseErr(out)

	if dryrun {
		return nil
	}
	return Bash(
		env,
		cmd, trace,
		out, errout)
}
