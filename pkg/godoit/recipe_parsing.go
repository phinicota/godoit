package godoit

import (
	"fmt"
	"strings"
)

// auxiliary function to cast interface to string slice
func interfaceToStringSlice(v interface{}) []string {
	vSlice := v.([]interface{})
	ret := []string{}
	for _, v := range vSlice {
		ret = append(ret, v.(string))
	}
	return ret
}

// auxiliary function to validate recipe configuration fields
func validConfField(field string) bool {
	validFields := map[string]struct{}{
		"RequiredEnvVars": struct{}{},
	}

	_, valid := validFields[field]
	return valid
}

// auxiliary function to validate single-field keys
func validateSingleField(key, validField string, dataMap map[string]interface{}) error {
	if len(dataMap) != 1 {
		extraFields := []string{}
		for field := range dataMap {
			if field != validField {
				extraFields = append(extraFields, field)
			}
		}
		return fmt.Errorf("%v can only specify \"%v\", extra fields: %v",
			key, validField, strings.Join(extraFields, ", "))
	}
	return nil
}

// UnmarshalTOML implements a custom unmarshalling method for recipe
func (recipe *Recipe) UnmarshalTOML(data interface{}) error {

	d, _ := data.(map[string]interface{})
	for key, dd := range d {

		switch key {

		case "Configuration":
			dmap := dd.(map[string]interface{})
			for field, val := range dmap {

				if !validConfField(field) {
					return fmt.Errorf("Configuration has invalid field \"%v\"",
						field)
				}

				switch field {
				case "RequiredEnvVars":
					for _, varr := range interfaceToStringSlice(val) {
						if _, exists := recipe.RequiredEnvVars[varr]; exists {
							return fmt.Errorf("RequiredEnvVar %v already exists", varr)
						}
						recipe.RequiredEnvVars[varr] = struct{}{}
					}
				}
			}

		case "Environment":
			envMap := dd.(map[string]interface{})
			for varr, val := range envMap {
				if _, exists := recipe.env[varr]; exists {
					return fmt.Errorf("{} is already defined in Environment")
				}
				recipe.env[varr] = val.(string)
				recipe.Environment = append(recipe.Environment,
					fmt.Sprintf("%v=%v", varr, val))
			}

		case "Pretarget", "Postarget", "Prestep", "Posstep":
			ptMap := dd.(map[string]interface{})

			if err := validateSingleField(key, "Actions", ptMap); err != nil {
				return err
			}

			if actions, ok := ptMap["Actions"]; ok {
				for _, val := range actions.([]interface{}) {
					switch key {
					case "Pretarget":
						recipe.PretargetActions = append(
							recipe.PretargetActions, Action(val.(string)))
					case "Postarget":
						recipe.PostargetActions = append(
							recipe.PostargetActions, Action(val.(string)))
					case "Posstep":
						recipe.PosstepActions = append(
							recipe.PosstepActions, Action(val.(string)))
					case "Prestep":
						recipe.PrestepActions = append(
							recipe.PrestepActions, Action(val.(string)))
					}
				}
			} else {
				return fmt.Errorf("%v must specify \"Actions\"", key)
			}

		case "Setup":
			setupMap := dd.(map[string]interface{})

			if err := validateSingleField(key, "Targets", setupMap); err != nil {
				return err
			}

			if actions, ok := setupMap["Targets"]; ok {
				for _, val := range actions.([]interface{}) {
					recipe.SetupTargets[val.(string)] = struct{}{}
				}
			} else {
				return fmt.Errorf("Pretarget must specify \"Targets\"")
			}

		default:
			// default is a target!
			if err := recipe.parseTarget(key, dd); err != nil {
				return err
			}
		}
	}

	return recipe.IsValid()
}

// Target parsing logic

// auxiliary function to validate target fields
func validTargetField(field string) bool {
	validFields := map[string]struct{}{
		"Actions":     struct{}{},
		"TargetDeps":  struct{}{},
		"Environment": struct{}{},
	}

	_, valid := validFields[field]
	return valid
}

func (recipe *Recipe) parseTarget(key string, dd interface{}) error {

	if _, ok := recipe.Targets[key]; ok {
		return fmt.Errorf("Target %v already exists", key)
	}

	targetData := dd.(map[string]interface{})
	for field := range targetData {
		if !validTargetField(field) {
			return fmt.Errorf("Target %v has invalid field \"%v\"",
				key, field)
		}
	}

	newTarget := NewTarget(key)
	if env, ok := targetData["Environment"]; ok {
		newTarget.Environment = append(newTarget.Environment,
			interfaceToStringSlice(env)...)
	}
	if deps, ok := targetData["TargetDeps"]; ok {
		newTarget.TargetDeps = interfaceToStringSlice(deps)
	}
	if actions, ok := targetData["Actions"]; ok {
		tostore := []Action{}
		for _, val := range actions.([]interface{}) {
			tostore = append(tostore, Action(val.(string)))
		}
		newTarget.Actions = tostore
	}

	if err := newTarget.IsValid(); err != nil {
		return err
	}

	if key != "Teardown" {
		recipe.Targets[key] = newTarget
	} else {
		if _, hasDeps := targetData["TargetDeps"]; hasDeps {
			return fmt.Errorf("Teardown target cannot have \"TargetDeps\" field")
		}
		recipe.TeardownTarget = newTarget
	}

	return nil
}
