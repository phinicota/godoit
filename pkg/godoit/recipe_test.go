package godoit

import (
	"os"
	"testing"

	"github.com/BurntSushi/toml"
)

type testRecipe struct {
	Recipe
	id string
}

func validRecipe1() testRecipe {
	res := testRecipe{
		id:     "validRecipe1",
		Recipe: Recipe{Targets: make(map[string]*Target)},
	}
	res.Targets["target1"] = &Target{
		name:       "target1",
		TargetDeps: []string{},
		Actions:    []Action{Action("ls -l")},
	}
	res.Targets["target2"] = &Target{name: "target2", TargetDeps: []string{"target1"}}
	res.Targets["target3"] = &Target{name: "target3", TargetDeps: []string{"target1", "target2"}}
	res.Targets["target4"] = &Target{name: "target4", TargetDeps: []string{"target1"}}
	return res
}

func validRecipeForEnvTest() testRecipe {
	res := testRecipe{
		id:     "envTestRecipe",
		Recipe: *NewRecipe(),
	}
	res.Recipe.Environment = append(res.Recipe.Environment, "testvar=recipe env")

	target1Name := "recipe env"
	res.Targets[target1Name] = NewTarget(target1Name)
	res.Targets[target1Name].Actions = append(res.Targets[target1Name].Actions,
		Action("test $testvar = recipe env"))

	target2Name := "overriden recipe env"
	res.Targets[target2Name] = NewTarget(target2Name)
	res.Targets[target2Name].Environment = append(res.Targets[target2Name].Environment,
		"testvar=target env")
	res.Targets[target2Name].Actions = append(res.Targets[target2Name].Actions,
		Action("test $testvar = target env"))
	return res
}

// reason: depends on undefined target
func invalidRecipe1() testRecipe {
	res := testRecipe{
		id:     "invalidRecipe1",
		Recipe: Recipe{Targets: make(map[string]*Target)},
	}
	res.Targets["target1"] = &Target{name: "target1", TargetDeps: []string{}}
	res.Targets["target2"] = &Target{name: "target2", TargetDeps: []string{"target1"}}
	res.Targets["target3"] = &Target{name: "target3", TargetDeps: []string{"target1", "target4"}}
	return res
}

var validRecipe2 = `
[Environment]
MYVAR="Hi"

[target1]
TargetDeps = ["target2", "target3"]
Actions = [
"echo Hi, Im target1!",
"test 4 -lt 6",
]

[target2]
TargetDeps = []
Actions = ["echo Hi, Im target2!",
"test ${TARGET_NAME} = target2",
]

[target4]
TargetDeps = []
Actions = ["echo Hi, Im target4!",
"test ${TARGET_NAME} = target4",
]

[target3]
TargetDeps = ["target2", "target4"]
Actions = [
"echo Hi, Im target3!",
"test 4 -lt 6",
]
`

var recipeTestCases = []struct {
	testingRecipe      testRecipe
	expectedIsValidNil bool
}{
	{validRecipe1(), true},
	{invalidRecipe1(), false},
}

func TestRecipeIsValid(t *testing.T) {
	for _, test := range recipeTestCases {
		result := test.testingRecipe.IsValid()
		if (result == nil && !test.expectedIsValidNil) ||
			(result != nil && test.expectedIsValidNil) {
			expectedString := "<non-nil>"
			if test.expectedIsValidNil {
				expectedString = "<nil>"
			}
			t.Fatalf("Recipe \"%v\" failed IsValid() test.\n"+
				"Expected: %v\nGot: %v",
				test.testingRecipe.id,
				expectedString, result)
		}
	}
}

func TestRecipeTomlParsing(t *testing.T) {
	var testRecipe = NewRecipe()
	if _, err := toml.Decode(validRecipe2, testRecipe); err != nil {
		t.Fatal(err)
	}

	if err := testRecipe.IsValid(); err != nil {
		t.Fatalf("Toml parsing test failed, recipe is not valid.\nError:%v", err)
	}

	if err := testRecipe.Run(NewCallContext(-1, false)); err != nil {
		t.Fatalf("recipe.Run() sequentially failed.\nError: %v", err)
	}

	testRecipe.Parallel = true
	for i := 1; i <= 100; i++ {
		if err := testRecipe.Run(NewCallContext(-1, false)); err != nil {
			t.Fatalf("recipe.Run() parallel try #%v failed.\nError: %v", i, err.Error())
		}
	}

	// // debug code
	// for _, target := range testRecipe.Targets {
	// 	t.Log(target.name, ":", target, target.Actions)
	// }

	// // toml debug code
	// buf := new(bytes.Buffer)
	// buf.Write([]byte("\n"))
	// if err := toml.NewEncoder(buf).Encode(recipeTestCases[0].testingRecipe); err != nil {
	// 	t.Log(err)
	// }
	// t.Log(buf.String())
}

// EqualContents compares two []string slices.
// returns true if their contents is the same (disregarding order)
func EqualContents(x, y []string) bool {

	valsx := map[string]struct{}{}

	for _, val := range x {
		valsx[val] = struct{}{}
	}

	for _, val := range y {
		if _, ok := valsx[val]; !ok {
			return false
		}
	}

	return len(valsx) == len(y)
}

func TestSolveOrder(t *testing.T) {
	recipe := validRecipe1()

	if err := recipe.Recipe.SolveExecutionOrder(); err != nil {
		t.Fatalf("SolveExecutionOrder failed for %v", recipe.id)
	}

	expectedExecSteps := []ExecutionStep{
		ExecutionStep([]string{"target1"}),
		ExecutionStep([]string{"target2", "target4"}),
		ExecutionStep([]string{"target3"}),
	}
	for k, resultStep := range recipe.Recipe.ExecutionSteps {
		expectedExecStep := expectedExecSteps[k]
		if !EqualContents([]string(resultStep), []string(expectedExecStep)) {
			t.Fatalf("ExecutionSteps test failed for step %v\n"+
				"Expected: %v\n"+
				"Got: %v",
				k, expectedExecStep, resultStep)
		}

	}

}

var missingReqEnvVarRecipe = `
[Configuration]
RequiredEnvVars=["undefinedvar"]

[Environment]
MYVAR="Hi"

[target1]
Actions = [
"echo Hi, Im target1!",
"test 4 -lt 6",
]
`

func TestMissingReqEnvVars(t *testing.T) {
	// test for required env vars in environment defined in recipe
	var testRecipe = NewRecipe()
	if _, err := toml.Decode(missingReqEnvVarRecipe, testRecipe); err == nil {
		t.Fatalf("Toml parsing invalid required env vars test should have failed but it didn't.")
	}

	// test for env vars defined before loading recipe
	var testRecipe2 = NewRecipe()
	if err := os.Setenv("definedvar2", "anything"); err != nil {
		t.Fatalf("unexpected error setting env var for test: %v", err)
	}
	if _, err := toml.Decode(missingReqEnvVarRecipe, testRecipe2); err == nil {
		t.Fatalf("Toml parsing invalid required env vars test should have failed but it didn't.")
	}
}

var repeatedEnvVarRecipe = `
[Environment]
REPEATEDVAR="Hi"
REPEATEDVAR="Hi"

[target1]
Actions = [
"echo Hi, Im target1!",
"test 4 -lt 6",
]
`

func TestRepeatedEnvVars(t *testing.T) {
	var testRecipe = NewRecipe()
	if _, err := toml.Decode(repeatedEnvVarRecipe, testRecipe); err == nil {
		t.Fatalf("Toml parsing repeated env vars test should have failed but it didn't.")
	}
}
