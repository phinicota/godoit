# godoit

[![Go Report Card](https://goreportcard.com/badge/gitlab.com/phinicota/godoit)](https://goreportcard.com/report/gitlab.com/phinicota/godoit) [![coverage report](https://gitlab.com/phinicota/godoit/badges/master/coverage.svg)](https://gitlab.com/phinicota/godoit/commits/master) [![pipeline status](https://gitlab.com/phinicota/godoit/badges/master/pipeline.svg)](https://gitlab.com/phinicota/godoit/commits/master) [![License MIT](https://img.shields.io/badge/License-MIT-brightgreen.svg)](https://img.shields.io/badge/License-MIT-brightgreen.svg) 

An automation tool heavily *inspired* by [pydoit](http://pydoit.org/) for concurrent execution of TOML format recipes.

### Package docs

https://godoc.org/gitlab.com/phinicota/godoit/pkg/godoit

### Command-line tool docs

https://godoc.org/gitlab.com/phinicota/godoit/cmd/godoit
