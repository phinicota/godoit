package main

import (
	"github.com/BurntSushi/toml"
	logging "github.com/sirupsen/logrus"
	"gitlab.com/phinicota/godoit/pkg/godoit"
	"gopkg.in/alecthomas/kingpin.v2"
	"os"
	"os/user"
	"path/filepath"
	"strings"
)

var (
	version     = "v0.1"
	dryrun      = kingpin.Flag("dryrun", "Show what would be done").Short('n').Default("false").Bool()
	verbosity   = kingpin.Flag("verbose", "Verbosity level. Can be repeated: -vvv").Short('v').Counter()
	recipePaths = kingpin.Arg("recipe_path", "Comma-separated paths to recipes to be executed. Looks for godoit.toml in current dir if none provided.").
			Default("godoit.toml").ExistingFiles()
	env         = kingpin.Flag("env", "Provide env vars like: -e MYVAR:MYVALUE -e MYVAR2=MYVALUE2").Short('e').Strings()
	notParallel = kingpin.Flag("no-parallel", "Will NOT run run recipes in parallel").Counter()
	hideStderr  = kingpin.Flag("hide-stderr", "Hides stderr from commands that DON'T exit with error").Counter()
)

type namedRecipe struct {
	path string
	*godoit.Recipe
}

func checkSingletonCounter(log *logging.Entry, varr *int, name string) {
	if *varr > 1 {
		log.Fatalf("use %v only once!", name)
	}
}

func warnRecipeOptsOverride(recipe namedRecipe,
	log *logging.Entry,
	notParallel, hideWarnings *int) {

	if *hideWarnings > 0 {
		if !recipe.HideWarnings {
			parameterName := "HideWarnings"
			log.Warnf("recipe %v default config forced, %v: %v -> %v",
				recipe.path, parameterName, recipe.HideWarnings, true)
		}
		recipe.HideWarnings = true
	}

	if *notParallel > 0 {
		if !recipe.Parallel {
			parameterName := "Parallel"
			log.Warnf("recipe %v default config forced, %v: %v -> %v",
				recipe.path, parameterName, recipe.Parallel, false)
		}
		recipe.Parallel = false
	}
}

func init() {
	kingpin.HelpFlag.Short('h')
	logging.SetOutput(os.Stdout)
}

func parseRecipeEnvVars(
	recipe *godoit.Recipe,
	log *logging.Entry) {

	for _, varr := range *env {
		splitted := strings.SplitN(varr, "=", 2)
		if len(splitted) != 2 {
			log.Fatalf("parsing environment variable failed: \"%v\"", varr)
		}

		// TODO remove '~' from vars
		// HACK https://stackoverflow.com/a/17617721
		if splitted[1] == "~" {
			log.Fatal("can't use home dir '~'")
		} else if strings.HasPrefix(splitted[1], "~/") {
			usr, err := user.Current()
			if err != nil {
				panic(err)
			}
			home := usr.HomeDir
			// Use strings.HasPrefix so we don't match paths like
			// "/something/~/something/"
			splitted[1] = filepath.Join(home, splitted[1][2:])
		}

		recipe.AddEnv(splitted[0], os.ExpandEnv(splitted[1]))
		log.Debugf("env %v=%v", splitted[0], os.ExpandEnv(splitted[1]))
	}
}

func main() {
	kingpin.Version(version)
	kingpin.Parse()

	callCtx := godoit.NewCallContext(*verbosity, *dryrun)
	log := godoit.LogCtx(callCtx)

	// check arguments
	checkSingletonCounter(log, notParallel, "--no-parallel")
	checkSingletonCounter(log, hideStderr, "--hide-stderr")

	log.Infof("check recipes...")
	var (
		recipes = []namedRecipe{}
	)
	// check recipes provided are valid
	for _, path := range *recipePaths {

		recipe := godoit.NewRecipe()

		parseRecipeEnvVars(recipe, log)

		log.Debugf("parse Recipe: %v", path)
		// check recipe file parses fine
		if _, err := toml.DecodeFile(path, &recipe); err != nil {
			log.Fatal(err)
		}

		recipes = append(recipes,
			namedRecipe{
				path:   path,
				Recipe: recipe})
	}

	log.Infof("will execute %v recipes: %v", len(recipes), *recipePaths)

	// actually execute recipes
	for _, recipe := range recipes {

		callCtx.LogFields["recipe"] = recipe.path

		log.Warnf("execute recipe: %v", recipe.path)
		recipe.ExecOptions.Trace = callCtx.Verbosity > 2
		warnRecipeOptsOverride(recipe, log,
			notParallel,
			hideStderr)

		// handle no-parallel flag
		if err := recipe.Run(callCtx); err != nil {
			log.Fatal(err)
		}

		log.Warnf("%v finished OK", recipe.path)
	}

	log.Infof("finished executing all recipes: %v", *recipePaths)
}
